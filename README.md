# 構成

- web
  - Nginx
  - Unicorn
  - Rails app
  - Ruby 2.1.2 (rbenv)
  - Redis
- db
  - mongodb


# 手順

- install Nginx
- copy nginx.conf from local
- start and enable nginx
- install ruby
- install rbenv and ruby-build
- install ruby 2.1.2 with rbenv
- install bundler gem (global)
- install Redis
- install mongodb
- start and enable mongodb
- deploy Rails app (with Capistrano or mina)

# その他

最初はdbも同じホストにインストールしてみる
